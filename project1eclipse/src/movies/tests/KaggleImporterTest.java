package movies.tests;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

import movies.importer.KaggleImporter;

// @author Sergio Segrera
class KaggleImporterTest {
	@Test
	void testKaggleImporter() {
		KaggleImporter importer = new KaggleImporter("", "");
		
		ArrayList<String> firstLine = new ArrayList<String>();
		firstLine.add("Brendan Fraser	John Hannah	Maria Bello	Michelle Yeoh	Jet Li	Russell Wong	\"The Fast and the Furious director Rob Cohen continues the tale set into motion by director Stephen Sommers with this globe-trotting adventure that finds explorer Rick O'Connell and son attempting to thwart a resurrected emperor's (Jet Li) plan to enslave the entire human race. It's been 2,000 years since China's merciless Emperor Han and his formidable army were entombed in terra cotta clay by a double-dealing sorceress (Michelle Yeoh), but now, after centuries in suspended animation, an ancient curse is about to be broken. Thanks to his childhood adventures alongside father Rick (Brendan Fraser) and mother Evelyn (Maria Bello), dashing young archeologist Alex O'Connell (Luke Ford) is more than familiar with the power of the supernatural. After he is tricked into awakening the dreaded emperor from his eternal slumber, however, the frightened young adventurer is forced to seek out the wisdom of his parents -- both of whom have had their ");
		
		ArrayList<String> firstMovie = importer.process(firstLine);
		
		ArrayList<String> firstLineProcessed = new ArrayList<String>();
		firstLineProcessed.add("The Fortune"+"\t"+"1975"+"\t"+"88"+"\t"+"kaggle");
		
		assertEquals(firstLineProcessed, firstMovie);
	}
}
