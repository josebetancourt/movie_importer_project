package movies.tests;
import movies.importer.Validator;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

//@author Jose Carlos Betancourt
class ValidatorTest {

	@Test
	void testValidator() {
		Validator newValidator = new Validator(" "," ");
		
		//Missing the release year
		ArrayList<String> firstLineNoYear = new ArrayList<String>();
		firstLineNoYear.add(""+"\t"+"1894"+"\t"+"45"+"\t"+"imdb");
		
		//Missing the name
		ArrayList<String> firstLineNoName = new ArrayList<String>();
		firstLineNoName.add("Miss Jerry"+"\t"+null+"\t"+"45"+"\t"+"imdb");
		
		//Missing the runtime
		ArrayList<String> firstLineNoTime = new ArrayList<String>();
		firstLineNoTime.add("Miss Jerry"+"\t"+"1894"+"\t"+""+"\t"+"imdb");
		
		//Invalid Year and RunTime
		ArrayList<String> firstLineInvYear = new ArrayList<String>();
		firstLineInvYear.add("Miss Jerry"+"\t"+"18b4"+"\t"+"45"+"\t"+"imdb");
		ArrayList<String> firstLineInvTime = new ArrayList<String>();
		firstLineInvTime.add("Miss Jerry"+"\t"+"184"+"\t"+"45o"+"\t"+"imdb");
		
		ArrayList<String> firstMovieNoYear = newValidator.process(firstLineNoYear);
		ArrayList<String> firstMovieNoName = newValidator.process(firstLineNoName);
		ArrayList<String> firstMovieNoTime = newValidator.process(firstLineNoTime);
		ArrayList<String> firstMovieInvYear = newValidator.process(firstLineInvYear);
		ArrayList<String> firstMovieInvTime = newValidator.process(firstLineInvTime);
		
		
		
		//Empty ArrayList
		ArrayList<String> firstLineProcessed = new ArrayList<String>();
		
		
		assertEquals(firstLineProcessed, firstMovieNoYear);
		assertEquals(firstLineProcessed, firstMovieNoName);
		assertEquals(firstLineProcessed, firstMovieNoTime);
		assertEquals(firstLineProcessed, firstMovieInvYear);
		assertEquals(firstLineProcessed, firstMovieInvTime);
		
	}

}
