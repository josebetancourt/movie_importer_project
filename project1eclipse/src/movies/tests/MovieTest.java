package movies.tests;
import movies.importer.Movie;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

//@author Jose Carlos Betancourt
class MovieTest {

	@Test
	void testGets() {
		Movie newMovie = new Movie("1994","Pulp Fiction","154","imdb");
		assertEquals("1994", newMovie.getReleaseYear());
		assertEquals("Pulp Fiction", newMovie.getMovieName());
		assertEquals("154", newMovie.getMovieRunTime());
		assertEquals("imdb", newMovie.getSource());
	}

}
