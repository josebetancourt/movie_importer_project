package movies.tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import movies.importer.Normalizer;

// @author Sergio Segrera
class NormalizerTest {
	@Test
	void testNormalizer() {
		Normalizer newNormalizer = new Normalizer("", "");
		ArrayList<String> stringList = new ArrayList<String>();
		
		stringList.add("The Fortune"+"\t"+"10/10/2000"+"\t"+"88"+"kaggle");
		
		ArrayList<String> firstMovie = newNormalizer.process(stringList);
		
		ArrayList<String> stringListNormalized = new ArrayList<String>();
		
		stringList.add("the fortune"+"\t"+"2000"+"\t"+"88"+"kaggle");
		
		assertEquals(stringListNormalized, firstMovie);
	}
}
