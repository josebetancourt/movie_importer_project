package movies.tests;
import movies.importer.Deduper;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

//@author Jose Carlos Betancourt
class DeduperTest {

	@Test
	void Dedupertest() {
		Deduper newDeduper = new Deduper(" ", " ");
		
		ArrayList<String> newArrList = new ArrayList<String>();
		newArrList.add(0,"Miss Jerry"+"\t"+"1894"+"\t"+"45"+"\t"+"imdb");
		newArrList.add(1,"Miss Jerry"+"\t"+"1894"+"\t"+"45"+"\t"+"imdb");
		
		ArrayList<String> arrListNoDoubles = newDeduper.process(newArrList);
		
		assertEquals("Miss Jerry"+"\t"+"1894"+"\t"+"45"+"\t"+"imdb", arrListNoDoubles.get(0));
		
		
	}

}
