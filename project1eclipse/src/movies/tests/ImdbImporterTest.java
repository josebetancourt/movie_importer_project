package movies.tests;
import movies.importer.ImdbImporter;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

//@author Jose Carlos Betancourt
class ImdbImporterTest {

	@Test
	void testImdbImporter() {
		ImdbImporter newProcessor = new ImdbImporter(" "," ");
		
		ArrayList<String> firstLine = new ArrayList<String>();
		firstLine.add("tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2");
		
		ArrayList<String> firstMovie = newProcessor.process(firstLine);
		
		ArrayList<String> firstLineProcessed = new ArrayList<String>();
		firstLineProcessed.add("Miss Jerry"+"\t"+"1894"+"\t"+"45"+"\t"+"imdb");
		
		assertEquals(firstLineProcessed, firstMovie);
		
	}

}
