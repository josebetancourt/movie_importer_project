package movies.importer;

import java.util.ArrayList;

// @author Sergio Segrera
public class Normalizer extends Processor {

	public Normalizer(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> newList = new ArrayList<String>();
		for (int i = 0; i < input.size(); i++) {
			String[] split = input.get(i).split("\\t");
			if (split.length == 4) {
				Movie movie = new Movie(split[1].substring(split[1].length() - 4), split[0].toLowerCase(), split[2].split(" ")[0], split[3]);
				newList.add(movie.toString());
			}
		}
		return newList;
	}

}
