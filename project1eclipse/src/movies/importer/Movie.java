package movies.importer;

//@author Jose Carlos Betancourt
public class Movie {
	private String releaseYear;
	private String movieName;
	private String movieRunTime;
	private String source;

	public Movie(String releaseYear, String movieName, String movieRunTime, String source) {
		this.releaseYear = releaseYear;
		this.movieName = movieName;
		this.movieRunTime = movieRunTime;
		this.source = source;
	}

	public String getReleaseYear() {
		return this.releaseYear;
	}

	public String getMovieName() {
		return this.movieName;
	}

	public String getMovieRunTime() {
		return this.movieRunTime;
	}

	public String getSource() {
		return this.source;
	}

	public String toString() {
		return (this.movieName + "\t" + this.releaseYear + "\t" + this.movieRunTime + "\t" + this.source);
	}
	
	// @author Sergio Segrera
	public boolean equals(Object object) {
		if (getClass() != object.getClass()) {
			System.out.println("false1");
			return false;
		}
		Movie movie = (Movie) object;
		int timeOne = Integer.parseInt(this.movieRunTime);
		int timeTwo = Integer.parseInt(movie.getMovieRunTime());
		int timeDifference = timeOne - timeTwo;
		
		if (this.movieName.equals(movie.getMovieName()) && this.releaseYear.equals(movie.getReleaseYear()) && timeDifference >= -5 && timeDifference <= 5) {
			return true;
		}

		return false;
	}

}
