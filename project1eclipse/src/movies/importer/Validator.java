package movies.importer;
import java.util.ArrayList;

//@author Jose Carlos Betancourt
public class Validator extends Processor {
	public Validator(String srcDir, String outputDir){
		super(srcDir, outputDir, false);
	}
	
	public ArrayList<String> process(ArrayList<String> arr_list){
		ArrayList<String> imdb_arr = new ArrayList<String>();
		
		for (int i=0; i<arr_list.size(); i++) {
			String movie = arr_list.get(i);
			String[] movie_arr = movie.split("\\t");
			if(movie_arr.length == 4) {
				String releaseYear = movie_arr[1];
				String movieName = movie_arr[0];
				String movieRunTime = movie_arr[2];
				String source = movie_arr[3];
				if(releaseYear.equals("") || releaseYear == null) {
					continue;
				}
				else {
				    try {
				        int year = Integer.parseInt(releaseYear);
				      } catch (Exception e) {
				        continue;
				      }
				}
				
				if(movieRunTime.equals("") || movieRunTime == null) {
					continue;
				}
				else {
				    try {
				        int time = Integer.parseInt(movieRunTime);
				      } catch (Exception e) {
				        continue;
				      }
				}
				
				if(movieName.equals("") || movieName == null) {
					continue;
				}
				Movie newMovie = new Movie(releaseYear, movieName, movieRunTime, source);
				movie = newMovie.toString();
				imdb_arr.add(movie);
			}
		}
		
		return imdb_arr;
	}
	
}
