package movies.importer;
import java.util.ArrayList;

//@author Jose Carlos Betancourt
public class ImdbImporter extends Processor {
	public ImdbImporter(String srcDir, String outputDir){
		super(srcDir, outputDir, true);
	}
	
	public ArrayList<String> process(ArrayList<String> arr_list){
		ArrayList<String> imdb_arr = new ArrayList<String>();
		
		for (int i=0; i<arr_list.size(); i++) {
			String movie = arr_list.get(i);
			String[] movie_arr = movie.split("\\t",-1);
			if(movie_arr.length == 22) {
				String releaseYear = movie_arr[3];
				String movieName = movie_arr[1];
				String movieRunTime = movie_arr[6];
				String source = "imdb";
				Movie newMovie = new Movie(releaseYear, movieName, movieRunTime, source);
				movie = newMovie.toString();
				imdb_arr.add(movie);
			}
		}
		
		return imdb_arr;
	}
	
}
