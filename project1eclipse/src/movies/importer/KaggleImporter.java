package movies.importer;

import java.util.ArrayList;


// @author Sergio Segrera
public class KaggleImporter extends Processor {

	public KaggleImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}

	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> newList = new ArrayList<String>();
		for (int i = 0; i < input.size(); i++) {
			String[] split = input.get(i).split("\\t");
			if (split.length == 21) {
				Movie movie = new Movie(split[12], split[15], split[13], "kaggle");
				newList.add(movie.toString());
			}
		}
		return newList;
	}

}
