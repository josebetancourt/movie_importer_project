package movies.importer;

import java.io.IOException;

//@author Jose Carlos Betancourt
public class ImportPipeLine {
	
	public static void processAll(Processor[] newProcessor) throws IOException {
		for(int i = 0; i < newProcessor.length; i++) {
			newProcessor[i].execute();
		}
	}

	public static void main(String[] args) throws IOException {
		String root = "C:\\Users\\uber\\eclipse-workspace\\movie_importer_project\\project1eclipse\\";
		String imdbSrcDir = root + "ImdbSmallFile";
		String kaggleSrcDir = root + "KaggleSmallFile";
		String importersOutputDir = root + "ImporterOutput";
		String normOutputDir = root + "NormalizerOutput";
		String valOutputDir = root + "ValidatorOutput";
		String dedOutputDir = root + "DeduperOutput";
		
		Processor[] newProcessor = new Processor[5];
		newProcessor[0] = new ImdbImporter(imdbSrcDir, importersOutputDir);
		newProcessor[1] = new KaggleImporter(kaggleSrcDir, importersOutputDir);
		newProcessor[2] = new Normalizer(importersOutputDir, normOutputDir);
		newProcessor[3] = new Validator(normOutputDir, valOutputDir);
		newProcessor[4] = new Deduper(valOutputDir, dedOutputDir);
		
		processAll(newProcessor);
		
		
	}

}
