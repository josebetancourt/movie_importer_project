package movies.importer;

import java.util.ArrayList;

// @author Sergio Segrera
public class Deduper extends Processor {

	public Deduper(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<Movie> movieList = new ArrayList<Movie>();
		for (int i = 0; i < input.size(); i++) {
			String[] split = input.get(i).split("\\t");
			if (split.length == 4) {
				Movie movie = new Movie(split[1], split[0], split[2], split[3]);
				if (movieList.contains(movie)) {
					int index = movieList.indexOf(movie);
					String source;
					if (movieList.get(index).getSource().equals(movie.getSource())) {
						source = movie.getSource();
					} else {
						source = movieList.get(index).getSource() + ";" + movie.getSource();
					}
					
					Movie mergedMovie = new Movie(
							movie.getReleaseYear(),
							movie.getMovieName(),
							movie.getMovieRunTime(),
							source
							);
					movieList.set(index, mergedMovie);
				} else {
					movieList.add(movie);
				}
			}
		}
		ArrayList<String> newList = new ArrayList<String>();
		for (int l = 0; l < movieList.size(); l++) {
			newList.add(movieList.get(l).toString());
		}
		return newList;
	}

}
